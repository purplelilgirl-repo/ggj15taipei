﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Shark : MonoBehaviour 
{
	float m_time = 0;
	public Text m_score;

	AudioSource[] m_audioSources;

	public AudioClip m_happyBGM;
	public AudioClip m_sadBGM;

	public AudioClip m_oceanSFX;

	public AudioClip m_warningSFX;
	public AudioClip m_cutScene2SFX;
	public AudioClip m_cutScene3SFX;
	public AudioClip m_cutScene4SFX;

	public AudioClip[] m_chompSFX;

	bool m_showCutScene = true;
	bool m_start = false;
	bool m_isAlive = true;
	bool m_hasFin = true;
	bool m_isShowingTutorial = true;
	bool m_isShowingCutScene = false;

	public Sprite m_normalSprite;
	public Sprite m_eatSprite;

	public Sprite m_finlessNormalSprite;
	public Sprite m_finlessEatSprite;

	public float[] m_weakSpeedArr;

	public float[] m_gravityArr;
	public float[] m_moveForceDashArr;
	public float[] m_moveForceUpArr;
	public float[] m_moveForceDownArr;
	public float[] m_sharkSpeedArr;

	public int m_currSetting = 0;

	[HideInInspector] public float m_health = 100;

	public GameObject m_cutScene1;
	public GameObject m_cutScene2;
	public GameObject m_cutScene3;
	public GameObject m_cutScene4;

	public GameObject m_pledge1;
	public GameObject m_pledge2;
	public GameObject m_pledge3;
	public GameObject m_pledge4;
	public GameObject m_pledge5;

	public UIScene m_uiScene;

	void Awake()
	{	
		Physics2D.gravity = new Vector2(0, m_gravityArr[m_currSetting]);
		gameObject.rigidbody2D.isKinematic = true;

		m_score.gameObject.SetActive(false);

		m_cutScene1.SetActive(false);
		m_cutScene2.SetActive(false);
		m_cutScene3.SetActive(false);
		m_cutScene4.SetActive(false);
		
		m_pledge1.SetActive(false);
		m_pledge2.SetActive(false);
		m_pledge3.SetActive(false);
		m_pledge4.SetActive(false);
		m_pledge5.SetActive(false);

		for (int i=0; i< 3; i++) 
		{	Camera.main.gameObject.AddComponent<AudioSource>();
		}

		m_audioSources = (AudioSource[]) Camera.main.gameObject.GetComponents<AudioSource>();

		playBGM(m_happyBGM);

		playOceanSFX();

	}

	void Update () 
	{
		if(m_start)
		{	
			if(m_isAlive)
			{	m_time += Time.deltaTime;

				transform.position += Vector3.right * Time.deltaTime * m_sharkSpeedArr[m_currSetting];

				m_health -= Time.deltaTime*m_weakSpeedArr[m_currSetting];
				if(m_health <= 0)
				{	m_health = 0;
					Die();
				}

				if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.PageUp))
				{	
					if(m_isShowingTutorial)
					{	m_isShowingTutorial = false;
						m_uiScene.HideTutorial();
					}

					rigidbody2D.AddForce(new Vector2(0, m_moveForceUpArr[m_currSetting]));
				}	

				else if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.PageDown))
				{	
					if(m_isShowingTutorial)
					{	m_isShowingTutorial = false;
						m_uiScene.HideTutorial();
					}

					rigidbody2D.AddForce(new Vector2(0, -m_moveForceDownArr[m_currSetting]));
				}

				/**
				else if(Input.GetKeyDown(KeyCode.Space))
				{	if(m_health >= 80)
					{	rigidbody2D.AddForce(new Vector2(m_moveForceDashArr[m_currSetting], 0));
					}
				}
				**/
			}

			else
			{	if(m_pledge4 && isSpriteTouch(m_pledge4))
				{	ReloadLevel();
				}
			}
		}

	}

	/**
	void OnCollisionEnter2D(Collision2D coll) 
	{

		if (coll.gameObject.tag == "ground")
		{	Debug.Log("DIE!");

			m_health = 0;

			Die();
		}

		if (coll.gameObject.tag == "bigfish")
		{	Debug.Log("DIE!");
			
			m_health = 0;

			Die();
		}
	}
	**/

	void playOceanSFX()
	{	playSFX2(m_oceanSFX);
	}

	void showNormalSprite()
	{	
		if(m_hasFin)
		{	transform.GetComponent<SpriteRenderer>().sprite = m_normalSprite;
		}	else
		{	transform.GetComponent<SpriteRenderer>().sprite = m_finlessNormalSprite;
		}

		LeanTween.rotateZ(gameObject, 0, 0.1f);
	}

	void showEatSprite()
	{	
		if(m_hasFin)
		{	transform.GetComponent<SpriteRenderer>().sprite = m_eatSprite;
		}	else
		{	transform.GetComponent<SpriteRenderer>().sprite = m_finlessEatSprite;
		}

		LeanTween.rotateZ(gameObject, 10, 0.1f);

		Invoke("showNormalSprite", 0.2f);
	}

	public void eatFish(GameObject fish)
	{	
		if(m_isAlive && !m_isShowingCutScene)
		{	m_health = Mathf.Clamp(m_health + fish.GetComponent<basic>().m_score, 0, 100);

			showEatSprite();
			
			fish.SetActive(false);

			playSFX(m_chompSFX[Random.Range(0, m_chompSFX.Length)]);
		}
	}

	public void StartGame()
	{	m_start = true;
		gameObject.rigidbody2D.isKinematic = false;

		if(m_showCutScene)
		{	Invoke("playCutSceneSFX", 14);
			Invoke("showCutScene", 15);
		}
	}

	void Die()
	{	m_isAlive = false;

		m_score.text = "You only survived " + m_time.ToString("F0") + " seconds.";
		m_score.gameObject.SetActive(true);

		Invoke("showPledge", 2);

		if(m_showCutScene)
		{	CancelInvoke("showCutScene");
		}
	}

	void ReloadLevel()
	{	Application.LoadLevel(Application.loadedLevel);
	}

	void playCutSceneSFX()
	{	playSFX2(m_warningSFX);
	}

	void showCutScene()
	{	
		m_isShowingCutScene = true;

		stopBGM();
			
		m_start = false;
		m_showCutScene = false;

		gameObject.rigidbody2D.isKinematic = true;

		showCutSceneAnim(m_cutScene1);

		Invoke("showCutScene2", 2);
	}

	void showCutScene2()
	{	
		showCutSceneAnim(m_cutScene2);
		Invoke("playCutScene2SFX", 0.2f);

		Invoke("showCutScene3", 2);
	}

	void playCutScene2SFX()
	{	playSFX2(m_cutScene2SFX);
	}

	void showCutScene3()
	{	
		showCutSceneAnim(m_cutScene3);
		Invoke("playCutScene3SFX", 0.2f);

		Invoke("showCutScene4", 2);
	}

	void playCutScene3SFX()
	{	playSFX2(m_cutScene3SFX);
	}

	void showCutScene4()
	{	
		showCutSceneAnim(m_cutScene4);
		Invoke("playCutScene4SFX", 0.2f);

		Invoke("hideCutSceneAndStartGame", 1);
	}

	void playCutScene4SFX()
	{	playSFX2(m_cutScene4SFX);
	}

	void showCutSceneAnim(GameObject cutScene)
	{	
		cutScene.transform.localScale = new Vector3(0, 0, 0);
		LeanTween.scale(cutScene, new Vector2(1.1f, 1.1f), 0.1f);
		LeanTween.scale(cutScene, new Vector2(1, 1), 0.05f).setDelay(0.1f);
		cutScene.SetActive(true);
	}

	void hideCutSceneAndStartGame()
	{	
		m_isShowingCutScene = false;

		playBGM(m_sadBGM);
		Invoke("playOceanSFX", Random.Range(2, 10));

		hideCutscene();

		m_hasFin = false;
		transform.GetComponent<SpriteRenderer>().sprite = m_finlessNormalSprite;

		m_currSetting = 1;

		StartGame();
	}

	void hideCutscene()
	{	
		m_cutScene1.SetActive(false);
		m_cutScene2.SetActive(false);
		m_cutScene3.SetActive(false);
		m_cutScene4.SetActive(false);
	}

	void showPledge()
	{	
		Debug.Log("showPledge");

		m_score.gameObject.SetActive(false);

		//stopBGM();
		CancelInvoke("playOceanSFX");

		showPledgeAnim(m_pledge5);

		Invoke("showPledge2", 0.2f);
	}

	void showPledge2()
	{
		showPledgeAnim(m_pledge1);
		
		Invoke("showPledge3", 1);
	}

	void showPledge3()
	{
		showPledgeAnim(m_pledge2);
		
		Invoke("showPledge4", 2);
	}

	void showPledge4()
	{
		showPledgeAnim(m_pledge3);
		
		Invoke("showPledge5", 1);
	}

	void showPledge5()
	{	showPledgeAnim(m_pledge4);
	}

	void showPledgeAnim(GameObject pledge)
	{	
		pledge.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
		LeanTween.alpha(pledge, 1, 0.1f);
		pledge.SetActive(true);
	}


	public bool isSpriteTouch(GameObject sprite) 
	{	return isSpriteTouch(sprite, 0);
	}
	
	public bool isSpriteTouch(GameObject sprite, int idx) 
	{	
		// inactive sprites do not receive touch
		if(!sprite.activeInHierarchy)
		{	return false;
		}
		
		else
		{	if(!Application.isEditor)
			{	
				if(isSpriteHitAtPos(getTouchPos(idx), sprite))
				{	return true;
				}
			}	
			// if editor, check for mouse position
			else
			{	if(isSpriteHitAtPos(getTouchPos(), sprite))
				{	return true;
				}
			}
		}
		
		return false;
	}
	
	bool isSpriteHitAtPos(Vector3 pos, GameObject sprite) 
	{	
		if(sprite.collider2D != null)
		{	RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector2.zero);
			
			foreach(RaycastHit2D hit in hits)
			{	if (hit && hit.collider != null) 
				{	if(hit.collider.gameObject == sprite)
					{	return true;
					}
				}
			}
		}	else
		{	if(sprite.renderer.bounds.Contains(new Vector3(pos.x, pos.y, sprite.transform.position.z)))
			{	return true;
			}	else
			{	return false;
			}
		}
		
		return false;
	}
	
	public Vector3 getTouchPos() 
	{
		return getTouchPos(0);
	}
	
	public Vector3 getTouchPos(int index) 
	{
		Vector2 touchInScreenPoint = getTouchScreenPos( index );
		
		return Camera.main.ScreenToWorldPoint ( new Vector3(touchInScreenPoint.x, 
		                                                    touchInScreenPoint.y, 0) );
	}
	
	public Vector2 getTouchScreenPos() 
	{
		return getTouchScreenPos(0);
	}
	
	public Vector2 getTouchScreenPos(int index) 
	{
		if( Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) || Input.GetMouseButtonDown(0)) 
		{   return Input.mousePosition;
		}
		
		return Vector2.zero;
	}

	void stopBGM()
	{	m_audioSources[0].Stop();
	}

	void playBGM(AudioClip clip)
	{	m_audioSources[0].clip = clip;
		m_audioSources[0].loop = true;
		m_audioSources[0].Play();
	}

	void playSFX(AudioClip clip)
	{	m_audioSources[1].clip = clip;
		m_audioSources[1].Play();
	}

	void playSFX2(AudioClip clip)
	{	m_audioSources[2].clip = clip;
		m_audioSources[2].Play();
	}
}
