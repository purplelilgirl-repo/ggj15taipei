﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScene : MonoBehaviour {

	public Shark m_shark; 

	public GameObject m_HPBox;
	public GameObject m_HPBar;
	public GameObject m_HPMask;
	public float m_HPMaskWidth;
	public GameObject m_startBtn;
	public GameObject m_startBtnPress;
	public float m_perMovePosX;
	public float m_addHPValue;
	public float m_hpScale;
	public bool m_isGameStart;
	public GameObject m_title;
	public GameObject m_tutorial;



	void Start () 
	{
		LeanTween.moveY (m_title, 0.7f, 0.3f).setOnComplete (ShowTitle);
		m_HPMask.transform.localScale = new Vector3 (0,
		                                             m_HPMask.transform.localScale.y,
		                                             0);
		m_isGameStart = false;
		m_startBtnPress.SetActive (false);
		m_hpScale = 0.01f;
		m_perMovePosX = 0.01f;

		m_shark.gameObject.SetActive(false);
		m_tutorial.SetActive (false);
	}

	void ShowTitle()
	{
		LeanTween.moveY (m_title,0.8f,0.1f);
//		m_title.SetActive (false);
		m_startBtn.SetActive (true);
	}

	void Update () 
	{

//		LeanTween.moveY (m_title, gameObject.transform.position.y + m_waterMoveOffset, m_waterMoveSpeed).setRepeat (-1).setLoopPingPong ();
		if (m_startBtn && isSpriteTouch (m_startBtn)) 
		{
			m_title.SetActive(false);
			m_startBtn.SetActive(false);
			m_startBtnPress.SetActive(true);
			Invoke("StartGame",0.2f);

		}

		/**
		if(m_isGameStart && m_HPMask.transform.localScale.x < 1.0f)
		{
			Debug.Log("localScale.x "+m_HPMask.transform.localScale.x+ " ,position.x "+ m_HPMask.transform.position.x);
			m_HPMask.transform.localScale = new Vector3(m_HPMask.transform.localScale.x + m_hpScale,m_HPMask.transform.localScale.y,0);
			m_HPMask.transform.position = new Vector3( m_HPMask.transform.position.x - m_perMovePosX,m_HPMask.transform.position.y,0);
		}
		**/

		UpdateHP();
	}

	public void StartTutorial()
	{
		m_tutorial.SetActive (true);
	}

	public void HideTutorial()
	{
		m_tutorial.SetActive (false);
	}

	public void UpdateHP () 
	{	
		float scale = (100-m_shark.m_health)/100;

		m_HPMask.transform.localScale = new Vector3(scale,m_HPMask.transform.localScale.y,0);

		//m_HPMask.transform.position = new Vector3( m_HPMask.transform.position.x - m_perMovePosX,m_HPMask.transform.position.y,0);

	}

	public void StartGame() 
	{
		m_isGameStart = true;
		m_startBtn.SetActive (false);
		m_startBtnPress.SetActive (false);
		m_tutorial.SetActive (true);

		m_HPBox.SetActive(true);
		m_HPBar.SetActive(true);
		m_HPMask.SetActive(true);

		m_shark.gameObject.SetActive(true);
		m_shark.StartGame();

		StartTutorial();
	}

	public bool isSpriteTouch(GameObject sprite) 
	{	return isSpriteTouch(sprite, 0);
	}
	
	public bool isSpriteTouch(GameObject sprite, int idx) 
	{	
		// inactive sprites do not receive touch
		if(!sprite.activeInHierarchy)
		{	return false;
		}
		
		else
		{	if(!Application.isEditor)
			{	
				if(isSpriteHitAtPos(getTouchPos(idx), sprite))
				{	return true;
				}
			}	
			// if editor, check for mouse position
			else
			{	if(isSpriteHitAtPos(getTouchPos(), sprite))
				{	return true;
				}
			}
		}
		
		return false;
	}

	bool isSpriteHitAtPos(Vector3 pos, GameObject sprite) 
	{	
		if(sprite.collider2D != null)
		{	RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector2.zero);
			
			foreach(RaycastHit2D hit in hits)
			{	if (hit && hit.collider != null) 
				{	if(hit.collider.gameObject == sprite)
					{	return true;
					}
				}
			}
		}	else
		{	if(sprite.renderer.bounds.Contains(new Vector3(pos.x, pos.y, sprite.transform.position.z)))
			{	return true;
			}	else
			{	return false;
			}
		}
		
		return false;
	}

	public Vector3 getTouchPos() 
	{
		return getTouchPos(0);
	}
	
	public Vector3 getTouchPos(int index) 
	{
		Vector2 touchInScreenPoint = getTouchScreenPos( index );
		
		return Camera.main.ScreenToWorldPoint ( new Vector3(touchInScreenPoint.x, 
		                                                    touchInScreenPoint.y, 0) );
	}

	public Vector2 getTouchScreenPos() 
	{
		return getTouchScreenPos(0);
	}
	
	public Vector2 getTouchScreenPos(int index) 
	{
		if( Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) || Input.GetMouseButtonDown(0)) 
		{   return Input.mousePosition;
		}
		
		return Vector2.zero;
	}
}
