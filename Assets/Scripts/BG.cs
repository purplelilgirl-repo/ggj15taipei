﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BG : MonoBehaviour 
{	public GameObject m_water;



	void Start () 
	{	//m_fish = new ArrayList();
	}

	public void generateFish()
	{	
	
		for(int i=0; i < 2; i++)
		{	
			GameObject fish = OGGenerateFishMgr.Instance.getRandomFish();
			Vector3 randomPos = getRandomPosition(fish);
			fish.transform.position = randomPos;

			fish.transform.parent = transform;
			//m_fish.Add(fish);
		}
	}

	Vector3 getRandomPosition(GameObject fish)
	{	
		float xPos = Random.Range(m_water.transform.position.x+m_water.renderer.bounds.size.x/2-fish.renderer.bounds.size.x, m_water.transform.position.x-m_water.renderer.bounds.size.x/2+fish.renderer.bounds.size.x);
		float yPos = Random.Range(m_water.transform.position.y+m_water.renderer.bounds.size.y*0.4f-fish.renderer.bounds.size.y, m_water.transform.position.y-m_water.renderer.bounds.size.y/2+fish.renderer.bounds.size.y);

		return new Vector3(xPos, yPos, 0);
	}
}
