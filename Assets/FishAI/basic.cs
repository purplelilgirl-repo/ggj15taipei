﻿using UnityEngine;
using System.Collections;


public class basic : MonoBehaviour {
	//************ tunning here
	public float FISH_MOVEX = 1;
	public float FISH_MOVEY= 0.1f;
	public float MOTION_X_DURATION = 2;
	public float MOTION_Y_DURATION = 0.7f;
	//************ tunning here
	public float m_score;

	void Start () {
		init();
		setBasicMotion (FISH_MOVEX, FISH_MOVEY, MOTION_X_DURATION, MOTION_Y_DURATION);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void setBasicMotion(float x, float y , float x_duration, float y_duration){
		LeanTween.cancel (gameObject);
		LeanTween.moveX (gameObject, gameObject.transform.position.x + x, x_duration).setRepeat (-1).setLoopPingPong ();
		LeanTween.moveY (gameObject, gameObject.transform.position.y + y, y_duration).setRepeat (-1).setLoopPingPong ();
	}

	public virtual void init(){
	}
}
