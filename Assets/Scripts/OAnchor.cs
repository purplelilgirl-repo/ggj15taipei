﻿using UnityEngine;
using System.Collections;

public enum AnchorX
{	None,
	Center,
	Left, 
	Right,
};

public enum AnchorY
{	None,
	Center,
	Top, 
	Bottom,
};

[ExecuteInEditMode]
public class OAnchor : MonoBehaviour 
{
	public AnchorX m_anchorX;
	public AnchorY m_anchorY;
	public float m_xPos;
	public float m_yPos;

	public Transform m_referenceTransform;
	
	void Awake () 
	{	
		setAnchor();
	}
	
	public void setAnchor()
	{	if(m_referenceTransform)
		{	setAnchor(this.transform, m_anchorX, m_anchorY, m_xPos, m_yPos, m_referenceTransform);
		}	else
		{	setAnchor(this.transform, m_anchorX, m_anchorY, m_xPos, m_yPos);
		}
	}

	public static void setAnchor(Transform objectTransform, AnchorX anchorX, AnchorY anchorY, float xPos, float yPos)
	{
		if(anchorX == AnchorX.Center) 
		{	objectTransform.position = new Vector3(OResolutionHandler.C().x+xPos, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Left) 
		{	objectTransform.position = new Vector3(OResolutionHandler.L()+xPos+OResolutionHandler.SpriteE(objectTransform).x, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Right) 
		{	objectTransform.position = new Vector3(OResolutionHandler.R()+xPos-OResolutionHandler.SpriteE(objectTransform).x, objectTransform.position.y, objectTransform.position.z);
		}
		
		if (anchorY == AnchorY.Center) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  OResolutionHandler.C().y+yPos, objectTransform.position.z);
		} 	else if (anchorY == AnchorY.Top) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  OResolutionHandler.T()+yPos-OResolutionHandler.SpriteE(objectTransform).y, objectTransform.position.z);
		}	else if (anchorY == AnchorY.Bottom) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  OResolutionHandler.B()+yPos+OResolutionHandler.SpriteE(objectTransform).y, objectTransform.position.z);
		}
	}

	public static void setAnchor(Transform objectTransform, AnchorX anchorX, AnchorY anchorY, float xPos, float yPos, Transform referenceTransform)
	{
		if(anchorX == AnchorX.Center) 
		{	objectTransform.position = new Vector3(referenceTransform.position.x+xPos, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Left) 
		{	objectTransform.position = new Vector3(referenceTransform.position.x-OResolutionHandler.SpriteE(referenceTransform).x+xPos+OResolutionHandler.SpriteE(objectTransform).x, objectTransform.position.y, objectTransform.position.z);
		}	else if (anchorX == AnchorX.Right) 
		{	objectTransform.position = new Vector3(referenceTransform.position.x+OResolutionHandler.SpriteE(referenceTransform).x+xPos-OResolutionHandler.SpriteE(objectTransform).x, objectTransform.position.y, objectTransform.position.z);
		}
		
		if (anchorY == AnchorY.Center) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  referenceTransform.position.y+yPos, objectTransform.position.z);
		} 	else if (anchorY == AnchorY.Top) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  referenceTransform.position.y+OResolutionHandler.SpriteE(referenceTransform).y+yPos-OResolutionHandler.SpriteE(objectTransform).y, objectTransform.position.z);
		}	else if (anchorY == AnchorY.Bottom) 
		{	objectTransform.position = new Vector3(objectTransform.position.x,  referenceTransform.position.y-OResolutionHandler.SpriteE(referenceTransform).y+yPos+OResolutionHandler.SpriteE(objectTransform).y, objectTransform.position.z);
		}
	}
	
	public void Update()
	{
		if (Application.isEditor && !Application.isPlaying)
		{	setAnchor();
		}
	}
}
