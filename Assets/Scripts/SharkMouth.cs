﻿using UnityEngine;
using System.Collections;

public class SharkMouth : MonoBehaviour 
{
	public Shark m_shark;

	void OnTriggerEnter2D(Collider2D coll) 
	{
		if(coll.gameObject.tag == "fish")
		{	m_shark.eatFish(coll.gameObject);
		}
	}
}
