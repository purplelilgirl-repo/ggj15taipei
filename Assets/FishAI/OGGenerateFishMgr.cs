﻿using UnityEngine;
using System.Collections;

public class OGGenerateFishMgr  : Singleton<OGGenerateFishMgr > 
{
	public GameObject generateFish1()
	{
		return OGResourceMgr.Instance.loadPrefabObj ("fish1");
	}

	public GameObject generateFish2()
	{
		return OGResourceMgr.Instance.loadPrefabObj ("fish2");
	}

	public GameObject generateFish3()
	{
		return OGResourceMgr.Instance.loadPrefabObj ("fish3");
	}

	public GameObject generateFish4()
	{
		return OGResourceMgr.Instance.loadPrefabObj ("fish4");
	}

	public GameObject generateFish5()
	{
		return OGResourceMgr.Instance.loadPrefabObj ("fish5");
	}

	public GameObject generateFish6()
	{
		return OGResourceMgr.Instance.loadPrefabObj ("fish6");
	}

	public GameObject generateFish7()
	{
		return OGResourceMgr.Instance.loadPrefabObj ("fish7");
	}

	public GameObject generateFish8()
	{
		return OGResourceMgr.Instance.loadPrefabObj ("fish8");
	}

	public GameObject getRandomFish()
	{
		switch (Random.Range (1, 8)) {
		case 1:
			return generateFish1();
		case 2:
			return generateFish2();
		case 3:
			return generateFish3();
		case 4:
			return generateFish4();
		case 5:
			return generateFish5();
		case 6:
			return generateFish6();
		case 7:
			return generateFish7();
		case 8:
			return generateFish8();
		}
		return generateFish2();
	}
}
