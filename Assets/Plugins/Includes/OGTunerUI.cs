﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TunerPlugin;
using UnityEngine.UI;

public class OGTunerUI : MonoBehaviour 
{
	
	public List<TunerParamUI> m_paramsUI;

	public Button m_playBtn;
	public Button m_resetBtn;
	public Button m_closeBtn;

	[HideInInspector] public Camera m_mainCamera;
	
	void Start () 
	{

		m_mainCamera = Camera.main;

		float paramUIYPos = 0.75f;

		for(int i=0; i < m_paramsUI.Count; i++)
		{	
			TunerParamUI paramUI = m_paramsUI[i];
			
			paramUI.m_slider.transform.position = new Vector3(m_mainCamera.pixelWidth * 0.5f,m_mainCamera.pixelHeight * paramUIYPos,0);
			paramUI.m_valueText.transform.position = new Vector3 (paramUI.m_slider.transform.position.x - m_mainCamera.pixelWidth * 0.34f, paramUI.m_slider.transform.position.y + m_mainCamera.pixelHeight * 0.07f ,0);
			paramUI.m_valueUnitText.transform.position = new Vector3 (paramUI.m_valueText.transform.position.x + m_mainCamera.pixelWidth * 0.42f, paramUI.m_valueText.transform.position.y,0);
			paramUI.m_paramNameText.transform.position = new Vector3 (paramUI.m_slider.transform.position.x, paramUI.m_slider.transform.position.y - m_mainCamera.pixelHeight * 0.08f, 0);
			
			paramUIYPos-= 0.2f;

			if(Tuner.Instance.m_params.Count > i)
			{	
				TunerParam param = Tuner.Instance.m_params[i];
				
				paramUI.m_paramNameText.text = param.m_name;
				paramUI.m_slider.minValue = param.m_min;
				paramUI.m_slider.maxValue = param.m_max;
				paramUI.m_slider.value = param.m_value;
				paramUI.m_valueUnitText.text = param.m_unit;
			}
		}
		
		m_closeBtn.transform.position = new Vector3 (m_mainCamera.pixelWidth * 0.08f,m_mainCamera.pixelHeight*0.95f, 0);
		m_playBtn.transform.position = new Vector3 (m_mainCamera.pixelWidth * 0.33f, m_mainCamera.pixelHeight*0.08f, 0);
		m_resetBtn.transform.position = new Vector3 (m_mainCamera.pixelWidth * 0.67f, m_mainCamera.pixelHeight*0.08f, 0);
	}

	public void SendParamToPlugin()
	{
		for(int i = 0;	i < m_paramsUI.Count; i++)
		{
			TunerParamUI paramUI = m_paramsUI[i];
			if(Tuner.Instance.m_params.Count > i)
			{	Tuner.Instance.m_params[i].m_value = float.Parse (paramUI.m_slider.value.ToString("F2"));
			}
		}
			
		Tuner.Instance.SendParam();
	}

	public void ResetParamsToOrigin()
	{

		Tuner.Instance.resetParam();

		for(int i = 0; i < m_paramsUI.Count; i++)
		{	
			TunerParamUI paramUI = m_paramsUI[i];
			
			if(Tuner.Instance.m_params.Count > i)
			{	paramUI.m_slider.value = Tuner.Instance.m_params[i].m_value;
			}
		}
	}

	public void CloseTunerUI()
	{
		Tuner.Instance.closeTuner ();
	}

	// Update is called once per frame
	void Update () 
	{
		for(int i = 0; i < m_paramsUI.Count; i++)
		{	
			TunerParamUI paramUI = m_paramsUI[i];
			
			paramUI.m_valueText.text = paramUI.m_slider.value.ToString("F2");
		}

	}
}
