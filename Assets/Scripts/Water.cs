﻿using UnityEngine;
using System.Collections;

public class Water : MonoBehaviour {

	public float m_waterMoveSpeed = 1;
	public float m_waterMoveOffset = 0.1f;

	void Start () 
	{	LeanTween.moveY (gameObject, gameObject.transform.position.y + m_waterMoveOffset, m_waterMoveSpeed).setRepeat (-1).setLoopPingPong ();
	}
}
