﻿using UnityEngine;
using System.Collections;

public class SharkCamera : MonoBehaviour 
{
	public GameObject m_shark;
	public float m_offset;

	void LateUpdate () 
	{	transform.position = new Vector3(m_shark.transform.position.x-m_offset, transform.position.y, transform.position.z);
	}
}
