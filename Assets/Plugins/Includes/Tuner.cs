﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class TunerParam
{	
	public string m_name;
	public float m_min;
	public float m_max;
	public float m_default;
	public float m_value;
	public string m_unit;
	
	public TunerParam(string name, float minValue, float value, float maxValue, float defaultValue, string unit)
	{	m_name = name;
		m_min = minValue;
		m_max = maxValue;
		m_value = value;
		m_default = defaultValue;
		m_unit = unit;
	}
}

[System.Serializable]
public class TunerParamUI
{	public Slider m_slider;
	public Text m_valueText;
	public Text m_valueUnitText;
	public Text m_paramNameText;
}

namespace TunerPlugin
{
	public class Tuner : Singleton<Tuner> 
	{
		public delegate void TunerCallback (List<TunerParam> tunerParams);
		public static TunerCallback m_tunerCallback = delegate {};

		public List<TunerParam> m_params;

		GameObject m_tunerUI;

		public void initMgr()
		{	m_tunerUI = (GameObject) Instantiate(Resources.Load ("OGTunerUI"));
			m_tunerUI.SetActive (false);
		}
		
		public void setParamValue(List<TunerParam> tunerParams)
		{	m_params = tunerParams;
		}
	
		public void resetParam() 
		{	for(int i=0; i< m_params.Count; i++)
			{	m_params[i].m_value = m_params[i].m_default;
			}
		}

		public void SendParam()
		{	m_tunerCallback(m_params);
		}

		public void showTuner()
		{	m_tunerUI.SetActive (true);
		}

		public void closeTuner()
		{	m_tunerUI.SetActive (false);
		}
	}
}
