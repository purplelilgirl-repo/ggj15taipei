﻿using UnityEngine;
using System.Collections;

public class collider : MonoBehaviour {
	public GameObject target;
	// Use this for initialization
	//******
	public float escapeDistance = 20f;
	public float escapeTime = 10f;
	public int escapeTimes = -1;
	public bool escapeSuccse = false;
	//******
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setEscape(float distance, float time, int times )
	{
		escapeDistance = distance;
		escapeTime = time;
		escapeTimes = times;
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "shark" && (escapeTimes == -1 || escapeTimes > 0)) {
			escapeTimes --;
			LeanTween.cancel (target);
			if(escapeSuccse){
				LTDescr d = LeanTween.moveX (target, target.transform.position.x + escapeDistance, escapeTime).setDestroyOnComplete(true);
				d.setOnComplete(destroy);
			}
			else
				LeanTween.moveX (target, target.transform.position.x + escapeDistance, escapeTime);
		}
	}

	void destroy(){
		Destroy (target);
	}
}
