﻿using UnityEngine;
using System.Collections;

public class BGMgr : MonoBehaviour 
{	public BG[] m_bg;

	void Start()
	{	m_bg[0].transform.position = new Vector3(0, 0, 0);

		for(int i=1; i< m_bg.Length; i++)
		{	m_bg[i].transform.position = new Vector3(m_bg[i-1].transform.position.x + m_bg[i-1].m_water.renderer.bounds.size.x, 0, 0);
			m_bg[i].generateFish();
		}
	}

	void Update()
	{	
		for(int i=0; i< m_bg.Length; i++)
		{	if(m_bg[i].transform.position.x+m_bg[i].m_water.renderer.bounds.size.x/2 < OResolutionHandler.L())
			{	int prevIdx;

				if(i == 0)
				{	prevIdx = m_bg.Length-1;
				}	else
				{	prevIdx = i-1;
				}

				m_bg[i].transform.position = new Vector3(m_bg[prevIdx].transform.position.x + m_bg[prevIdx].m_water.renderer.bounds.size.x, 0, 0);

				//m_bg[i].destroyFish();
				m_bg[i].generateFish();
			}
		}
	}
}
