﻿// Contributors: Michelle

using UnityEngine;
using System.Collections;

public class OGResourceMgr : Singleton<OGResourceMgr> 
{
	public AudioClip loadAudioClip( string clipName ) 
	{	return (AudioClip)Resources.Load (clipName) as AudioClip;
	}
	
	public GameObject loadPrefab( string prefabName ) 
	{	return (GameObject) Resources.Load (prefabName);
	}
	
	public GameObject loadPrefabObj( string prefabName ) 
	{	return (GameObject) Instantiate(Resources.Load (prefabName));
	}

	public Sprite loadSprite( string spriteName ) 
	{	return (Sprite) Resources.Load<Sprite>(spriteName);
	}

	public GameObject LoadSpriteObj( string spriteName ) 
	{	
		GameObject obj = new GameObject();
		obj.name = spriteName;
		obj.AddComponent<SpriteRenderer>();
		obj.GetComponent<SpriteRenderer>().sprite = (Sprite) Resources.Load<Sprite>(spriteName);
		return obj;
	}
	
	// NOTE: Michelle: for Multiple Spritesheets:
	private Hashtable m_spritesCache;
	
	// ATTENTION: Michelle:spritsheet needs to be initialized first and foremost, initSpritesheet only accepts Multiple sprites
	public void initSpritesheet(string spritesheetName) 
	{		
		if (m_spritesCache == null) 
		{	m_spritesCache = new Hashtable();		
		}
		
		Sprite[] spritesheet = Resources.LoadAll<Sprite>(spritesheetName);
		
		foreach (Sprite sprite in spritesheet) 
		{	
			// sprites with same names are ignored
			if(!m_spritesCache.Contains(sprite.name))
			{	m_spritesCache.Add(sprite.name, sprite);
			}
		}
	}
	
	// ATTENTION: Michelle: always clear spritesheet cache when changing Scenes
	public void clearSpritesheetCache()
	{	m_spritesCache.Clear();
	}	
	
	public Sprite loadSpriteFromSpritesheet(string name) 
	{	if (m_spritesCache.Contains (name)) 
		{	return (Sprite) m_spritesCache[name];		
		}
		
		return null;
	}
	
	public GameObject loadSpriteObjFromSpritesheet( string spriteName ) 
	{	
		GameObject obj = new GameObject();
		obj.name = spriteName;
		obj.AddComponent<SpriteRenderer>();
		obj.GetComponent<SpriteRenderer>().sprite = loadSpriteFromSpritesheet(spriteName);
		return obj;
	}
}
