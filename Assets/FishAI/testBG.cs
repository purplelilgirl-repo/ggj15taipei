﻿using UnityEngine;
using System.Collections;

public class testBG : MonoBehaviour {
	public GameObject m_water;
	
	ArrayList m_fish;
	
	public GameObject m_fishPrefab;
	
	void Start () 
	{	m_fish = new ArrayList();
	}
	
	public void generateFish()
	{	
		/**
		if(m_fish.Count > 0)
		{	for(int i=0; i < m_fish.Count; i++)
			{	
				if(m_fish[i] != null)
				{	Destroy((GameObject) m_fish[i]);
				}	
			}

			m_fish.Clear();
		}
		**/
		
		for(int i=0; i < 3; i++)
		{	GameObject fish = (GameObject) Instantiate(m_fishPrefab);
			
			Vector3 randomPos = getRandomPosition(fish);
			fish.transform.position = randomPos;
			
			fish.transform.parent = transform;
			//m_fish.Add(fish);
		}
	}
	
	Vector3 getRandomPosition(GameObject fish)
	{	
		float xPos = Random.Range(m_water.transform.position.x+m_water.renderer.bounds.size.x/2-fish.renderer.bounds.size.x, m_water.transform.position.x-m_water.renderer.bounds.size.x/2+fish.renderer.bounds.size.x);
		float yPos = Random.Range(m_water.transform.position.y+m_water.renderer.bounds.size.y/2-fish.renderer.bounds.size.y, m_water.transform.position.y-m_water.renderer.bounds.size.y/2+fish.renderer.bounds.size.y);
		
		return new Vector3(xPos, yPos, 0);
	}
}
